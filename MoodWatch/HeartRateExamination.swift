//
//  HeartRateExaminnation.swift
//  MoodWatch
//
//  Created by JinhoYoo on 2015. 8. 23..
//  Copyright © 2015 Zoomma. All rights reserved.
//

import Foundation


//HeatRateExamination
//
//Based on http://www.healthy-heart-meditation.com/target-heart-rate.html and http://www.polar.com/us-en/support/How_to_calculate_target_heart_rate_zone_ ,
//examine user's heart rate is light, moderate, or vigorous using 'Age based formula'.


class HeartRateExamination{
 
    func ageBasedExam(age:Int, heartRate:Int) -> String{
        
        let maximumHartRate:Int = 220 - age
        
        var zone1:[Int] = [ maximumHartRate * 6/10, maximumHartRate*7/10 ]
        var zone2:[Int] = [ maximumHartRate * 7/10, maximumHartRate*8/10 ]
        var zone3:[Int] = [ maximumHartRate * 8/10, maximumHartRate ]
        
        
        var status:String
       
        switch heartRate{
            
        case zone1[0]...zone1[1]:
            status = "light"

        case zone2[0]...zone2[1]:
            status = "moderate"

        case zone3[0]...zone3[1]:
            status = "vigorous"
            
        default:
            status = ""
        
        }
        return status
    }
    
}