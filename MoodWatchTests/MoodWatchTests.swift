//
//  MoodWatchTests.swift
//  MoodWatchTests
//
//  Created by JinhoYoo on 2015. 6. 27..
//  Copyright © 2015년 Zoomma. All rights reserved.
//

import XCTest


class MoodWatchTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHeartRateExamination_ageBasedExam() {
        
        let hrExam:HeartRateExamination = HeartRateExamination()
        let result :String = hrExam.ageBasedExam( 30, heartRate: 120)
        assert(result == "light")
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
}
